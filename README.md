# Latest Download #
[lnk]: https://bitbucket.org/eastmad/floor-13-releases/downloads/f13redux250517.exe "Latest f13redux Windows executable"
[img]: http://2.bp.blogspot.com/-JqDMwBw7FlM/T7kQRuFyf0I/AAAAAAAAAJU/V0vYvbEshDo/s1600/download_icon_small.png 
[![Latest download is Windows 64bit][img]][lnk]

## May 2017 (Manchester release)
* Many more location pictures
* Polls and Diary
* Another damage level

## Feb 2017 ##
* You can replay the last game, or try a new game
* Many more headshots now
* More of the game cycle

(The 'new game' will not be saved, as there is no installation of the game as yet)

## Oct 2016 ##
* About 15 available plots
* Some new art (not much)
* Game cycle with dates and sackings possible


## 15th January ##
* More of the same
* Summary

## 06th October ##
* About 6 plots
* Still nothing from infiltration
* This uses Carl Cropleys original graphics

## 04th October ##
* Yay, the first MAC app version! Unzip and open the App via the finder.
* About 6 plots
* Still nothing from infiltration
* This uses Carl Cropleys original graphics
* It will probably fail to launch if you don't have an openssl library. So it favours Homebrew users or developers at the moment. Will fix soon.


## 24th March ##
* Three plots, coming in random order.
* The game will last about 15-20, based on the lifetime of the plot.
* You can use all of the departments to some degree, except infiltration
* See [blog post](http://agriculture-and-fisheries.tumblr.com/post/114604446201/yes-the-new-bad-alpha-is-here)

## 1st December ##
* Single plot, see [Tumblr blog and Plot Deep dive](http://agriculture-and-fisheries.tumblr.com/post/102106186436/plot-deep-dive-part-one)
* The game will only run for a few turns, based on the lifetime of the plot.
* You can use most of the departments to some degree, except the ones that deal with groups


# What is this #

This is the download area for the lastest bad pre-alpha build of Floor13.
If you are here, you probably do know what [Floor13](http://en.wikipedia.org/wiki/Floor_13_(video_game)) was; well this is me slowly remaking it.

The executables show some playable progress, but only noddy graphics.

At the moment, there is only a Windows 64 bit executable.
(I could produce Mac version, if that is unavoidable)

# How to play #
Download the executable somewhere before running it.
It's keyboard menu driven. 

As it's just using your Windows command prompt shell , you can change the font from the window icon menu itself:

    properties -> font

 "Lucida Console" for example.
 
# Play testing feedback #

The usual rules apply - what you notice is lacking is more important than what I think is missing. Right now, that will be rather a lot of things missing, of course.

You can raise an issue on this butbucket site (someone already has) and that's good too. **If it crashes immediately, I'd obviously like very much to know!**

If you so wish, you can leave feedback back on http://agriculture-and-fisheries.tumblr.com; that would be great.

There should be a log written to the same place that the executable runs from - if you get a nice crash, it should be recorded there.